# persistent

Type-safe, multi-backend data serialization. [persistent](https://hackage.haskell.org/package/persistent)

# Books
* Developing Web Apps with Haskell and Yesod, 2nd Edition (Ch. 10)

# Unofficial documentation
* [*Databases and Persistent*
  ](https://mmhaskell.com/real-world/databases)
  (2022) Monday Morning Haskell
